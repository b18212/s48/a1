//! Mock database
let posts = [];

//! count varriable that will serve as the post ID
let count = 1;

//! Add post data
//* e.preventDefault();
// prevents the page from loading
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
    e.preventDefault();
    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value,
    });
    console.log(count);
    // Increment the id value in count variable
    count++;

    showPosts(posts);
    alert("Successfully added.");
});

//! Show posts
const showPosts = (posts) => {
    // Sets the html structure to display new posts
    let postEntries = "";

    posts.forEach((post) => {
        postEntries += `
      <div id="div-post-entries">
          <h3 id="post-title-${post.id}">${post.title}</h3>
          <p id="post-body-${post.id}">${post.body}</p>
          <button onclick="editPost('${post.id}')">Edit</button>
          <button onclick="deletePost('${post.id}')">Delete</button>
      </div>
          `;
    });

    document.querySelector("#div-post-entries").innerHTML = postEntries;
};

//! Edit post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
};

//! Update Post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();

    /*
posts[0].id = 1;
posts[0].title = Inception;
posts[0].body = Scifi;

txt-edit-id = 1
txt-edit-title = Anabelle
txt-edit-body = Horror
posts[0].id = 1 === #txt-edit-id.value = 1

post.length = 3;
posts[i] == 0 .id = 1

    */
    for (let i = 0; i < posts.length; i++) {
        if (
            posts[i].id.toString() === document.querySelector("#txt-edit-id").value
        ) {
            posts[i].title = document.querySelector("#txt-edit-title").value;
            posts[i].body = document.querySelector("#txt-edit-body").value;

            showPosts(posts);
            document.querySelector("#txt-edit-title").value = "";
            document.querySelector("#txt-edit-body").value = "";
            alert("Successfully updated");
            break;
        }
    }
});

//! Delete Post

const deletePost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).value;
    let body = document.querySelector(`#post-body-${id}`).value;

    document.querySelector("#txt-edit-id").value = id;

    for (let i = 0; i < posts.length; i++) {
        if (
            posts[i].id.toString() === document.querySelector("#txt-edit-id").value
        ) {
            posts.splice(i, 1);
            showPosts(posts);
            title = "";
            body = "";
            alert("Successfully deleted.");
            break;
        }
    }
};

//* OTHER SOLUTION
// const deletePost = (id) => {
//     for (let i = 0; i < posts.length; i++) {
//         if (posts[i].id.toString() === id) {
//             posts.splice(i, 1);
//             showPosts(posts);
//             alert("Successfully deleted.");
//         }
//     }
// };

//* OTHER SOLUTION
// const deletePost = (id) => {
//     for (let i = 0; i < posts.length; i++) {
//         if (posts[i].id === Number(id)) {
//             posts.splice(i, 1);
//             showPosts(posts);
//             document.querySelector("#txt-edit-title").value = "";
//             document.querySelector("#txt-edit-body").value = "";
//             break;
//         }
//     }
// };